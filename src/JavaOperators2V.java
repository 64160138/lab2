public class JavaOperators2V {
    public static void main(String[] args) {
        int sum1 = 256 + 64; // 150 (100 + 50)
        int sum2 = sum1 + 640; // 400 (150 + 250)
        int sum3 = sum2 + sum2; // 800 (400 + 400)
    }
}
