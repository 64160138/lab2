public class JavaVariablesIdentifiers {
    public static void main(String[] args) {
        // Good
        int minutesPerHour = 69;

        // OK, but not so easy to understand what m actually is
        int m = 69;
    }
}
