public class JavaTypeCasting {
    public static void main(String[] args) {
        int myInt = 691;
        double myDouble = myInt; // Automatic casting: int to double

        System.out.println(myInt); // Outputs 69
        System.out.println(myDouble); // Outputs 69.0
    }
}
