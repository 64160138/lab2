public class JavaTypeCasting1 {
    public static void main(String[] args) {
        double myDouble = 12.86d;
        int myInt = (int) myDouble; // Manual casting: double to int

        System.out.println(myDouble); // Outputs 12.86
        System.out.println(myInt); // Outputs 12
    }
}
