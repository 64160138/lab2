public class JavaDataTypes {
    public static void main(String[] args) {
        int myNum = 99; // Integer (whole number)
        float myFloatNum = 99.12f; // Floating point number
        char myLetter = 'D'; // Character
        boolean myBool = true; // Boolean
        String myText = "Hello"; // String
    }
}
