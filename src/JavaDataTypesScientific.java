public class JavaDataTypesScientific {
    public static void main(String[] args) {
        float f1 = 29e7f;
        double d1 = 12E10d;
        System.out.println(f1);
        System.out.println(d1);
    }
}
